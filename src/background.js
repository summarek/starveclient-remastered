import { app, protocol, BrowserWindow, Menu, ipcMain, webFrame } from 'electron';
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';
const fs = require("fs");
const url = require("url");
const path = require("path");
const isDevelopment = process.env.NODE_ENV !== 'production';
let windows = {
  application: null
};
protocol.registerSchemesAsPrivileged([{
  scheme: 'app',
  privileges: {
    secure: true,
    standard: true
  }
}]);
let create = {
  application: function() {
    windows.application = new BrowserWindow({
      width: 960,
      height: 600,
      frame: true,
      webPreferences: {
        nodeIntegration: true,
        webviewTag: true,
      }
    });
    if (process.env.WEBPACK_DEV_SERVER_URL) {
      windows.application.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
      //if (!process.env.IS_TEST) windows.application.webContents.openDevTools()
    } else {
      createProtocol('ui');
      windows.application.loadURL('ui://./index.html');
    }
    windows.application.on('closed', () => {
      windows.application = null
    });
    windows.application.setTitle("starveClient");
    Menu.setApplicationMenu(null);
  },
};
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});
app.on('activate', () => {
  if (windows.application === null) {
    create.application();
  }
});
app.on('ready', async () => {
  if (process.platform === "win32") {
    app.setPath("appData", path.join(process.env.APPDATA, app.getName()));
  }
  create.application();
});
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    });
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    });
  }
}
