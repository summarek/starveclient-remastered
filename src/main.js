import Vue from 'vue';
import Vuex from 'vuex';
import VueCountryFlag from 'vue-country-flag';
import VueTippy from 'vue-tippy';
import VueNotification from 'vue-notification';
import VuePerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
//
import App from './App.vue';
import router from './routes/index';
import modal from './components/Modal.vue';
import i18n from './languages/index';
//
Vue.use(Vuex);
Vue.component('country-flag', VueCountryFlag);
Vue.use(VueTippy);
Vue.use(VueNotification);
Vue.use(VuePerfectScrollbar);
//
const renderApp = new Vue({
  el: '#app',
  template: '<App/>',
  components: {App},
  router,
  i18n,
  render: h => h(App)
});

