const languages = {
    en: {
        // menu
        menu: {
            serverslist: "Servers list"
        },
        // buttons
        buttons: {
            playnow: "Play now"
        },
        // actions
        actions: {
            settings: "Settings",
            exit: "Exit"
        },
        // servers
        servers: {
            title: "Servers list",
            infoplayers: "Players",
            infoservers: "Servers",
            search: "Search",
            table_name: "Server name",
            table_players: "Players",
            table_gamemode: "Gamemode",
            table_ping: "Ping",
            filter_hide_full: "Hide full",
            filter_hide_empty: "Hide empty",
            filter_refresh: "Refresh"
        },
        settings: {
            title: "Settings",
            language_title: "Language",
            nickname_title: "Nickname",
            nickname_placeholder: "Player",
            gamesettings_title: "Game settings",
            gamesettings_zoom: "Game zoom"
        }
    },
    pl: {
        // menu
        menu: {
            serverslist: "Lista serwerów"
        },
        // actions
        actions: {
            settings: "Ustawienia",
            exit: "Wyjdź"
        },
        // buttons
        buttons: {
            playnow: "Zagraj teraz"
        },
        // servers
        servers: {
            title: "Lista serwerów",
            infoplayers: "Gracze",
            infoservers: "Serwery",
            search: "Szukaj",
            table_name: "Nazwa serwera",
            table_players: "Gracze",
            table_gamemode: "Tryb gry",
            table_ping: "Ping",
            filter_hide_full: "Ukryj pełne",
            filter_hide_empty: "Ukryj puste",
            filter_refresh: "Odśwież"
        },
        settings: {
            title: "Ustawienia",
            language_title: "Język",
            nickname_title: "Pseudonim",
            nickname_placeholder: "Gracz",
            gamesettings_title: "Ustawienia gry",
            gamesettings_zoom: "Skalowanie gry"
        }
    },
    ru: {
        // menu
        menu: {
            serverslist: "Список серверов"
        },
        // buttons
        buttons: {
            playnow: "Играть"
        },
        // actions
        actions: {
            settings: "Настройки",
            exit: "Выход"
        },
        // servers
        servers: {
            title: "Список серверов",
            infoplayers: "Игроки",
            infoservers: "Серверы",
            search: "Поиск",
            table_name: "Имя сервера",
            table_players: "Игроки",
            table_gamemode: "Игровой режим",
            table_ping: "Ping",
            filter_hide_full: "Полностью скрыть",
            filter_hide_empty: "Скрыть пустое",
            filter_refresh: "Обновление"
        },
        settings: {
            title: "Настройки",
            language_title: "Язык",
            nickname_title: "Прозвище",
            nickname_placeholder: "Игрок",
            gamesettings_title: "Настройки игры",
            gamesettings_zoom: "Game zoom"
        }  
    },
};

export default languages;