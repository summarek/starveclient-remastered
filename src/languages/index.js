import Vue from 'vue';
import VueI18n from 'vue-i18n';
import languages from './languages';

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: "en",
    fallbackLocale: "pl",
    messages: languages
});

export default i18n;