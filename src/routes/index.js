import Vue from 'vue';
import VueRouter from 'vue-router';
//
import Servers from './Servers.vue';
import Settings from './Settings.vue';
//
Vue.use(VueRouter);
//
const router = new VueRouter({
    linkExactActiveClass: "active",
    routes: [
        {
            path: "/",
            alias: "/index.html",
            component: Servers,
            name: "Home"
        },
        {
            path: "/settings",
            component: Settings,
            name: "Settings"
        }
    ],
});
export default router;